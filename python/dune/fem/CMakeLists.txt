add_subdirectory(discretefunction)
add_subdirectory(function)
add_subdirectory(model)
add_subdirectory(operator)
add_subdirectory(scheme)
add_subdirectory(space)
add_subdirectory(utility)

add_library(_fem SHARED _fem.cc)
set_target_properties(_fem PROPERTIES PREFIX "")
target_link_libraries(_fem dunefem )
add_python_targets(fem
  __init__
  _adaptation
  _spaceadaptation
  plotting
  view
)
